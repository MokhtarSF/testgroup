<?php

namespace App\EventSubscriber;

use App\Entity\Group;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

/**
 * This is a class listener to all doctrine events and we it only act on "Group" entity
 *
 * Class GroupEventSubscriber
 * @package App\EventListener
 */
class GroupEventSubscriber implements EventSubscriber
{

    /**
     * @var TokenStorage
     */
    private $tokenStorage;

    /**
     * GroupEventSubscriber constructor
     * @param TokenStorage $tokenStorage
     */
    public function __construct(TokenStorage $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * We define here the doctrine events that our subscriber will act on
     *
     * @return array of events
     */
    public function getSubscribedEvents()
    {
        return [
            'prePersist',
        ];
    }

    /**
     * At this Event doctrine is about to run the INSERT query:
     *
     *  1- we catch then object
     *
     *  2- check if it is a Group object (as doctrine will dispatch this event each time
     *      an object is going to be persisted)
     *
     *  3- set the owner of group
     *  4- set createdAt
     *
     * @param LifecycleEventArgs $eventArgs object with the event arguments
     */
    public function prePersist(LifecycleEventArgs $eventArgs)
    {
        $entity = $eventArgs->getEntity();

        // We only want to act on "Group" entity
        if (!$entity instanceof Group) {
            return;
        }

        $entity->setOwner($this->tokenStorage->getToken()->getUser());
        $entity->setCreatedAt(new \DateTime());
    }

}