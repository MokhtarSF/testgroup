<?php

namespace App\EventSubscriber;

use App\Entity\UserGroup;
use App\Manager\NotificationManager;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Event\PostFlushEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

/**
 * This is a class listener to all doctrine events and we it only act on "UserGroup" entity
 *
 * Class UserGroupEventSubscriber
 * @package App\EventListener
 */
class UserGroupEventSubscriber implements EventSubscriber
{
    /**
     * @var TokenStorage
     */
    private $tokenStorage;

    /**
     * @var NotificationManager
     */
    private $notificationManager;

    private $joinRequests = [];

    private $acceptRequests = [];

    private $refuseRequests = [];

    /**
     * UserGroupEventSubscriber constructor
     *
     * @param TokenStorage $tokenStorage
     * @param NotificationManager $notificationManager
     */
    public function __construct(
        TokenStorage $tokenStorage,
        NotificationManager $notificationManager
    )
    {
        $this->tokenStorage = $tokenStorage;
        $this->notificationManager = $notificationManager;
    }

    /**
     * We define here the doctrine events that our subscriber will act on
     *
     * @return array of events
     */
    public function getSubscribedEvents()
    {
        return [
            'prePersist',
            'preUpdate',
            'onFlush',
            'postFlush',
        ];
    }

    /**
     * At this Event doctrine is about to run the INSERT query:
     *
     *  1- we catch then object
     *
     *  2- check if it is a UserGroup object (as doctrine will dispatch this event each time
     *      an object is going to be persisted)
     *
     *  3- set the user
     *
     * @param LifecycleEventArgs $eventArgs object with the event arguments
     */
    public function prePersist(LifecycleEventArgs $eventArgs)
    {
        $entity = $eventArgs->getEntity();

        // We only want to act on "UserGroup" entity
        if (!$entity instanceof UserGroup) {
            return;
        }

        $entity->setUser($this->tokenStorage->getToken()->getUser());

    }

    /**
     * At this Event doctrine is about to run the UPDATE query:
     *
     *  1- we catch then object
     *
     *  2- check if it is a UserGroup object (as doctrine will dispatch this event each time
     *      an object is going to be updated)
     *
     *  3- check status has change or not
     *  4- if case new value of status is accepted
     *  5- update number of members group
     *
     * @param PreUpdateEventArgs $eventArgs object with the event arguments
     */
    public function preUpdate(PreUpdateEventArgs $eventArgs)
    {
        $entity = $eventArgs->getEntity();

        // We only want to act on "UserGroup" entity
        if (!$entity instanceof UserGroup) {
            return;
        }

        // We only want to act if status is changed
        if ($eventArgs->hasChangedField('status')){

            //When status changed to ACCEPTED
            if ($eventArgs->getNewValue('status') == UserGroup::STATUS_ACCEPTED) {

                // Increment number of members
                $group = $entity->getGroup();
                $group->setCount($group->getCount() +1);

            }

        }

    }

    /**
     * At this Event doctrine is about to run the COMMIT query:
     *
     *  1- We catch all UserGroup scheduled for insertions in an array
     *
     *  2- We catch all UserGroup scheduled for updates in an array
     *
     * @param OnFlushEventArgs $eventArgs object with the event arguments
     */
    public function onFlush(OnFlushEventArgs $eventArgs)
    {
        $entityManager = $eventArgs->getEntityManager();

        $uow = $entityManager->getUnitOfWork();

        foreach ($uow->getScheduledEntityInsertions() as $entity) {


            // We only want to act on "UserGroup" entity
            if (!$entity instanceof UserGroup) {
                continue;
            }

            $this->joinRequests[] = $entity;
        }

        foreach ($uow->getScheduledEntityUpdates() as $entity) {

            // We only want to act on "UserGroup" entity
            if (!$entity instanceof UserGroup) {
                continue;
            }

            $changes = $uow->getEntityChangeSet($entity);

            if (array_key_exists('status', $changes)) {

                if(end($changes['status']) == UserGroup::STATUS_ACCEPTED) {
                    $this->acceptRequests[] = $entity;
                }

                if(end($changes['status']) == UserGroup::STATUS_REFUSED) {
                    $this->refuseRequests[] = $entity;
                }
            }
        }

    }


    /**
     * At this Event the COMMIT query is done and no errors or exception happened
     *  and all created data has been persisted
     *
     *  1- We will submit notification for each item in array
     *
     * @param PostFlushEventArgs $eventArgs object with the event arguments
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function postFlush(PostFlushEventArgs $eventArgs)
    {
        $entityManager = $eventArgs->getEntityManager();

        if (count($this->joinRequests)) {
            $this->notificationManager->notifyJoinRequest($entityManager, $this->joinRequests);
            $this->joinRequests = [];
            $entityManager->flush();
        }

        if (count($this->acceptRequests)) {
            $this->notificationManager->notifyAcceptRequest($entityManager, $this->acceptRequests);
            $this->acceptRequests = [];
            $entityManager->flush();
        }

        if (count($this->refuseRequests)) {
            $this->notificationManager->notifyRefuseRequest($entityManager, $this->refuseRequests);
            $this->refuseRequests = [];
            $entityManager->flush();
        }
    }

}