<?php

namespace App\Controller;

use App\Entity\Group;
use App\Entity\Notification;
use App\Entity\User;
use App\Entity\UserGroup;
use App\Form\GroupType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Route("/notification")
 */
class NotificationController extends Controller
{

    /**
     * @Route("/", name="notification_index", methods="GET")
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     */
    public function index(): Response
    {
        $user = $this->getUser();
        $notifications = $this->getDoctrine()
            ->getRepository(Notification::class)
            ->findBy([
                'receiver' => $user,
                'status' => false
            ]);

        return $this->render('notification/index.html.twig', ['notifications' => $notifications]);
    }
}
