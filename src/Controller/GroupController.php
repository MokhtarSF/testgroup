<?php

namespace App\Controller;

use App\Entity\Group;
use App\Entity\User;
use App\Entity\UserGroup;
use App\Form\GroupType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
/**
 * @Route("/")
 */
class GroupController extends Controller
{
    /**
     * @Route("/", name="group_search", methods="GET")
     * @param Request $request
     * @return Response
     */
    public function search(Request $request): Response
    {
        $groups = null;
        $key = $request->query->get('key', null);
        if ($key) {
            $groups = $this->getDoctrine()
                ->getRepository(Group::class)
                ->findGroupsByNameOrType($key);
        }
        return $this->render('group/search.html.twig', ['key' => $key, 'groups' => $groups]);
    }

    /**
     * @Route("/list", name="group_index", methods="GET")
     * @return Response
     */
    public function index(): Response
    {
        $groups = $this->getDoctrine()
            ->getRepository(Group::class)
            ->findAll();

        return $this->render('group/index.html.twig', ['groups' => $groups]);
    }

    /**
     * @Route("/new", name="group_new", methods="GET|POST")
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {
        $group = new Group();
        $form = $this->createForm(GroupType::class, $group);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($group);
            $em->flush();

            return $this->redirectToRoute('group_index');
        }

        return $this->render('group/new.html.twig', [
            'group' => $group,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/join", name="group_join", methods="GET")
     * @param Group $group
     * @return Response
     */
    public function join(Group $group): Response
    {
        $user = $this->getUser();

        if ($user == $group->getOwner()) {
            return new Response('You are the admin of this group');
        }

        if ($userGroup = $this->getUserGroup($user, $group)) {
            return new Response($this->getMessage($userGroup));
        }

        $userGroup =  new UserGroup();
        $userGroup->setGroup($group);

        $em = $this->getDoctrine()->getManager();
        $em->persist($userGroup);
        $em->flush();

        return new Response('Join Request Sent');

    }

    /**
     * @Route("/{id}/accept", name="group_accept", methods="GET")
     * @param UserGroup $userGroup
     * @return Response
     */
    public function accept(UserGroup $userGroup): Response
    {
        $user = $this->getUser();
        $group = $userGroup->getGroup();

        if ($user != $group->getOwner()) {
            $this->createAccessDeniedException();
        }

        $userGroup->setStatus(UserGroup::STATUS_ACCEPTED);

        $em = $this->getDoctrine()->getManager();
        $em->persist($userGroup);
        $em->flush();

        return new Response('Join Request Accepted');

    }

    /**
     * @Route("/{id}/refuse", name="group_refuse", methods="GET")
     * @param UserGroup $userGroup
     * @return Response
     */
    public function refuse(UserGroup $userGroup): Response
    {
        $user = $this->getUser();
        $group = $userGroup->getGroup();

        if ($user != $group->getOwner()) {
            $this->createAccessDeniedException();
        }

        $userGroup->setStatus(UserGroup::STATUS_REFUSED);

        $em = $this->getDoctrine()->getManager();
        $em->persist($userGroup);
        $em->flush();

        return new Response('Join Request Refused');

    }


    /**
     * @Route("/{id}/show", name="group_show", methods="GET")
     * @param Group $group
     * @return Response
     */
    public function show(Group $group): Response
    {
        $em = $this->getDoctrine()->getManager();
        $usersGroup = $em->getRepository(UserGroup::class)->findBy([
            'group' => $group,
            'status' => UserGroup::STATUS_ACCEPTED
        ]);
        return $this->render('group/show.html.twig', ['group' => $group, 'usersGroup' => $usersGroup]);
    }


    /**
     * @param User $user
     * @param Group $group
     * @return UserGroup
     */
    private function getUserGroup(User $user, Group $group): UserGroup
    {
        $em = $this->getDoctrine()->getManager();
        $userGroup = $em->getRepository(UserGroup::class)->findOneBy([
            'user' => $user,
            'group' => $group
        ]);
        return $userGroup;
    }

    /**
     * This function return a string message depending of status user group
     * @param UserGroup $userGroup
     * @return string
     */
    private function getMessage(UserGroup $userGroup) :string
    {
        if ($userGroup->getStatus() == UserGroup::STATUS_DEFAULT) {
            return "You are already send join request to this group";
        }

        if ($userGroup->getStatus() == UserGroup::STATUS_ACCEPTED) {
            return "You are already member of this group";
        }

        return  "You refused to join this group";
    }
}
