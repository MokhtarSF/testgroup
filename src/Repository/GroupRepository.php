<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

class GroupRepository extends EntityRepository
{
    public function findGroupsByNameOrType($key)
    {
        return $this->createQueryBuilder('g')
        ->where('g.name LIKE :key')
        ->orWhere('g.type LIKE :key')
        ->setParameter('key', '%'.$key.'%')
        ->getQuery()
        ->getResult();
    }
}