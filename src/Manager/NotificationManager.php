<?php

namespace App\Manager;

use App\Entity\Notification;
use App\Entity\User;
use App\Entity\UserGroup;
use Doctrine\ORM\EntityManager;

class NotificationManager {

    /**
     * @param array $joinRequests
     * @param EntityManager $entityManager
     */
    public function notifyJoinRequest(EntityManager $entityManager, array $joinRequests)
    {
        /** @var UserGroup $userGroup */
        foreach ($joinRequests as $userGroup) {
            $group = $userGroup->getGroup();
            $requester = $userGroup->getUser();
            $owner = $group->getOwner();

            $messageToAdmin = $requester->getFullName() .' want to join the group ' . $group->getName();

            $this->sendNotification($entityManager, $messageToAdmin, $owner, $userGroup);

        }
    }

    /**
     * @param array $acceptRequests
     * @param EntityManager $entityManager
     */
    public function notifyAcceptRequest(EntityManager $entityManager, array $acceptRequests)
    {
        /** @var UserGroup $userGroup */
        foreach ($acceptRequests as $userGroup) {
            $group = $userGroup->getGroup();
            $requester = $userGroup->getUser();

            $messageToReceiver = $group->getName() .' accepted your join request.';

            $this->sendNotification($entityManager, $messageToReceiver, $requester, $userGroup);

            $messageToMember = $requester->getFullName() . ' joined the group ' .$group->getName();

            foreach ($group->getUserGroup() as $ug) {
                if ($ug->getUser() != $requester) {
                    $this->sendNotification($entityManager, $messageToMember, $ug->getUser(), $ug);
                }
            }
        }

    }

    /**
     * @param array $refuseRequests
     * @param EntityManager $entityManager
     */
    public function notifyRefuseRequest(EntityManager $entityManager, array $refuseRequests)
    {
        /** @var UserGroup $userGroup */
        foreach ($refuseRequests as $userGroup) {
            $group = $userGroup->getGroup();
            $requester = $userGroup->getUser();

            $messageToReceiver = $group->getName() .' refused your join request.';

            $this->sendNotification($entityManager, $messageToReceiver, $requester, $userGroup);

            $messageToMember = $requester->getFullName() . ' has been refused in the ' .$group->getName();

            foreach ($group->getUserGroup() as $ug) {

                if ($ug->getUser() != $requester) {
                    $this->sendNotification($entityManager, $messageToMember, $ug->getUser(), $ug);
                }
            }
        }
    }

    /**
     * @param EntityManager $entityManager
     * @param string $message
     * @param User $receiver
     * @param UserGroup $userGroup
     */
    private function sendNotification(EntityManager $entityManager, string $message, User $receiver, UserGroup $userGroup)
    {
        $notification = new Notification();
        $notification->setMessage($message);
        $notification->setReceiver($receiver);
        $notification->setUserGroup($userGroup);

        $entityManager->persist($notification);
    }


}