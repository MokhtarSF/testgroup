<?php

namespace App\Tests\EventSubscriber;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\Event\LifecycleEventArgs;
use App\Entity\Group;
use App\EventSubscriber\GroupEventSubscriber;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class GroupEventSubscriberTest extends WebTestCase
{

    function testPrePersistWithNonGroupObjectSuccess()
    {

        $om = $this->getMockBuilder(ObjectManager::class)->getMock();
        $storage = $this->getMockBuilder(TokenStorage::class)->getMock();

        $group = new Group();

        $date = new \DateTime();

        $group->setCreatedAt($date);

        $subscriber = new GroupEventSubscriber($storage);

        $arguments = new LifecycleEventArgs(new \DateTime(), $om);

        $subscriber->prePersist($arguments);

        $this->assertEquals($date, $group->getCreatedAt());

    }

}