<?php

namespace App\Tests\Form;

use App\Entity\Group;
use App\Form\GroupType;
use Symfony\Component\Form\Test\TypeTestCase;

class GroupTypeTest extends TypeTestCase
{
    public function testSubmitValidData()
    {
        $formData = array(
            'name' => 'test',
            'type' => 'test2',
        );

        $objectToCompare = new Group();
        $form = $this->factory->create(GroupType::class, $objectToCompare);

        $object = new Group();
        $object->setName($formData['name']);
        $object->setType($formData['type']);

        $form->submit($formData);

        $this->assertTrue($form->isSynchronized());

        $this->assertEquals($object, $objectToCompare);

        $view = $form->createView();
        $children = $view->children;

        foreach (array_keys($formData) as $key) {
            $this->assertArrayHasKey($key, $children);
        }
    }
}