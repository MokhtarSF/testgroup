<?php

namespace App\Tests\Form;

use App\Form\UserType;
use App\Entity\User;
use Symfony\Component\Form\Test\TypeTestCase;

class UserTypeTest extends TypeTestCase
{
    public function testSubmitValidData()
    {
        $formData = array(
            'firstName' => 'test',
            'lastName' => 'test2',
            'username' => 'test2',
            'plainPassword' => 'test2',
        );

        $objectToCompare = new User();
        $form = $this->factory->create(UserType::class, $objectToCompare);

        $object = new User();
        $object->setFirstName($formData['firstName']);
        $object->setLastName($formData['lastName']);
        $object->setUsername($formData['username']);
        $object->setPlainPassword($formData['plainPassword']);

        $form->submit($formData);

        $this->assertTrue($form->isSynchronized());

        $this->assertEquals($object, $objectToCompare);

        $view = $form->createView();
        $children = $view->children;

        foreach (array_keys($formData) as $key) {
            $this->assertArrayHasKey($key, $children);
        }
    }
}