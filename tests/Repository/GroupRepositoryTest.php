<?php

namespace App\Tests\Repository;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class GroupRepositoryTest extends KernelTestCase
{

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    /**
     * {@inheritDoc}
     */
    public function setUp()
    {
        self::bootKernel();
        $this->em = static::$kernel->getContainer()
            ->get('doctrine')
            ->getManager()
        ;
    }

    public function testFindGroupsByNameOrType()
    {
        $queryBuilder = $this->em->getRepository('App:Group')
            ->findGroupsByNameOrType('n0tf0und');
        ;
        /*$this->assertEquals('g.name LIKE :key', (string) $queryBuilder->getDqlPart()['where']);
        $this->assertEquals('g.type LIKE :key', (string) $queryBuilder->getDqlPart('orWhere'));
        $this->assertEquals(array('key' => 'g'), $queryBuilder->getParameters());*/

        $this->assertCount(0, $queryBuilder);
    }
}