<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class GroupControllerTest extends WebTestCase
{
    public function testRedirectLogin()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/');

        $this->assertTrue($client->getResponse()->isRedirect());
        $this->assertEquals(302, $client->getResponse()->getStatusCode());

        $this->assertGreaterThan(
            0,
            $crawler->filter('html:contains("Redirecting")')->count()
        );
    }

    public function testSearch()
    {
        $client = static::createClient();

        $this->logIn($client);

        $crawler = $client->request('GET', '/');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertSame('Search', $crawler->filter('label')->text());
    }

    public function testList()
    {
        $client = static::createClient();

        $this->logIn($client);

        $crawler = $client->request('GET', '/list');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertGreaterThan(
            0,
            $crawler->filter('html:contains("List Groups")')->count()
        );
    }

    public function testNew()
    {
        $client = static::createClient();

        $this->logIn($client);

        $crawler = $client->request('GET', '/new');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertGreaterThan(
            0,
            $crawler->filter('html:contains("Create new Group")')->count()
        );
    }

    public function testSearchNoGroupsFound()
    {
        $client = static::createClient();

        $this->logIn($client);

        $crawler = $client->request('GET', '/');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $form = $crawler->selectButton('search')->form(['key' => 'gt'], 'GET');

        $crawler = $client->submit($form);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $this->assertSame('No groups found', $crawler->filter('h6')->text());
    }

    private function logIn($client)
    {
        $session = $client->getContainer()->get('session');

        $firewallName = 'main';
        $firewallContext = 'main';

        $token = new UsernamePasswordToken('admin', null, $firewallName, array('ROLE_USER'));
        $session->set('_security_'.$firewallContext, serialize($token));
        $session->save();

        $cookie = new Cookie($session->getName(), $session->getId());
        $client->getCookieJar()->set($cookie);
    }
}
